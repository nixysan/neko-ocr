﻿from cloudmersive_ocr_api_client.rest import ApiException
import cloudmersive_ocr_api_client
import telegram
from telegram import ChatAction, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext.dispatcher import run_async
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
import logging
import os
import json
from functools import wraps

##########################################################################################################


# api key
API_KEY = os.environ.get(
    "CLOUDMERSIVE_API", "API Key")

###########################################################################################################
# bot typing (https://core.telegram.org/method/messages.setTyping)


def send_typing_action(func):
    """Sends typing action while processing func command."""
    # wraps
    @wraps(func)
    def command_func(update, context, *args, **kwargs):
        context.bot.send_chat_action(
            chat_id=update.effective_message.chat_id, action=ChatAction.TYPING)
        return func(update, context,  *args, **kwargs)

    return command_func


##########################################################################################################
# logging pada bot
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

###########################################################################################################
# BOT send typing


@run_async
@send_typing_action
def start(update, context):
    """Send a message when the command /start is issued."""
    global first
    first = update.message.chat.first_name  # get fname user
    update.message.reply_text('Hi! '+str(first)+' \n\nENG : Welcome to Optical Character Recognition (OCR) Bot.\nJust send a clear image to me and i will recognize the text in the image and send it as a message!\n\nID : Selamat datang di Optical Character Recognition (OCR) Bot.\nKirimkan saja gambar yang jelas kepada saya dan saya akan mengenali teks dalam gambar tersebut dan mengirimkannya sebagai pesan!')


# get file img dari user
@run_async
@send_typing_action
def user_image(update, context):
    global filename
    filename = "userimg.jpg"
    global file_id
    file_id = update.message.photo[-1].file_id
    newFile = context.bot.get_file(file_id)
    # download img
    newFile.download(filename)

    global chat_id
    chat_id = update.message.chat_id

    # inline keyboard yang berisi query bahasa
    keyboard = [[InlineKeyboardButton("English", callback_data='ENG'),  InlineKeyboardButton("Russian", callback_data='RUS'), InlineKeyboardButton("Chinese simplified", callback_data='ZHO')],
                [InlineKeyboardButton("Japanese", callback_data='JPA'), InlineKeyboardButton("Indonesian", callback_data='IND'), InlineKeyboardButton("Arabic", callback_data='ARA')]]

    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text(
        'Yeayy I got the Image please wait, I would be process it 😺\nPlease Select Language : ', reply_markup=reply_markup)
    return user_image


@run_async
def button(update, context):
    global query
    query = update.callback_query
    query.answer()
    query.edit_message_text(text="Selected Language is: {}".format(
        query.data))  # get query.data callback_query dari keyboard[]

    # membuat configurasi api instance
    configuration = cloudmersive_ocr_api_client.Configuration()
    # get api key
    configuration.api_key['Apikey'] = API_KEY
    api_instance = cloudmersive_ocr_api_client.ImageOcrApi(
        cloudmersive_ocr_api_client.ApiClient(configuration))
    try:  # pengkondisisan
        lang = query.data  # lang = query.data dari callback_query dari keyboard[]
        api_response = api_instance.image_ocr_post(
            filename, language=lang)
        # get confidance level for calculate percentage
        confidence = api_response.mean_confidence_level
        result = api_response.text_result  # get response from OCR api
        context.bot.send_message(
            chat_id=chat_id, text="Yeay, here the result 😻\nI Confidence : "+str(confidence*100)+"% \nExtracted text:\n\n<pre style='text-align:justify;display:inline;margin:0'>"+str(result)+"</pre>", parse_mode="HTML")
    except ApiException as e:
        context.bot.send_message(
            chat_id=chat_id, text="Exception when calling ImageOcrApi->image_ocr_photo_to_text: %s\n" % e)
        try:
            os.remove('userimg.jpg')
        except Exception:
            pass
    return button


def main():
    bot_token = os.environ.get(
        "BOT_TOKEN", "Token")  # Telegram BOT API (t.me/NixyOCR_bot)
    updater = Updater(bot_token, use_context=True)
    dp = updater.dispatcher
    # command inline pada bot (https://core.telegram.org/bots/inline)
    dp.add_handler(CommandHandler('start', start))
    # handler untuk memproses img
    dp.add_handler(MessageHandler(Filters.photo, user_image))
    dp.add_handler(CallbackQueryHandler(button))
    updater.start_polling()
    updater.idle()

 ############################################################################################################


if __name__ == "__main__":
    main()
